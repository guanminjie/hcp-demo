import { Component } from '@angular/core';
import { HotCodePush } from '@ionic-native/hot-code-push/ngx';

declare let window:any;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  update:string[] = [];

  constructor(private hotCodePush: HotCodePush) {}

  checkNewVersion() {
    this.hotCodePush.fetchUpdate().then( () => {
      return this.hotCodePush.getVersionInfo()
    }).then( (data) => {
      alert(`当前版本${data.currentWebVersion}，  上一版本${data.previousWebVersion}，待更新版本${data.readyToInstallWebVersion}`)
    })
    this.hotCodePush.onUpdateInstalled().subscribe( (data) => {alert(data.details.error.code)} )
  }

  installUpdate() {
    this.hotCodePush.isUpdateAvailableForInstallation().then( data => { 
      return this.hotCodePush.installUpdate();
    }).then(
      (data) => {
        alert("update installed");
      }
    )  
  }

  getCurrentVersion() {
    this.hotCodePush.getVersionInfo().then(
      (data) => {
        alert(`当前版本${data.currentWebVersion}，上一版本${data.previousWebVersion}，待更新版本${data.readyToInstallWebVersion}`)
      })
  }
  
}
